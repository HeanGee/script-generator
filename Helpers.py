import configparser
import os
import os.path
import re
import sys
from os import path
from pathlib import Path

# Symbols
group_syms = ['(', ')', '[', ']', '-', ':']


def get_short(string):
    # Abbreviations
    shortens = {
        "#": "Numb",
        "account": "Acc",
        "accounts": "Accs",
        "cloud": "Cloud",
        "communities": "Comms",
        "community": "Comm",
        "customer": "Cust",
        "digital": "Digi",
        "einstein": "Einst",
        "email": "Email",
        "engagement": "Engag",
        "health": "Health",
        "inbox": "Inb",
        "industry": "Industry",
        "license": "Lic",
        "licenses": "Lics",
        "lightning": "Light",
        "marketing": "Mrktng",
        "messaging": "Msg",
        "numeric": "Num",
        "order": "Ord",
        "orders": "Ords",
        "platform": "Platf",
        "potential": "Pot",
        "quantities": "Qtys",
        "quantity": "Qty",
        "quip": "Quip",
        "sale": "Sale",
        "sales": "Sales",
        "salesforce": "SF",
        "sandbox": "Sndb",
        "sector": "Sector",
        "segment": "Segm",
        "service": "Serv",
        "services": "Servs",
        "status": "Status",
    }

    key = string.lower()

    if key in shortens:
        return shortens[key].lower()

    return string[:4].lower()


def get_all_shortened(tokens=None):
    return map(get_short, tokens) if tokens else None


def paragraph_input(section=None, key='play', prompt_header="", tabul=""):
    """Read from stdin user inputs as a paragraph and return it."""

    buffer = []
    while True:
        print(f"{prompt_header} (. to finish)> ", end="")
        line = input_wrapper(section, key, '')

        if line == ".":
            break

        buffer.append(f"{tabul}{line}")

        # Implement use of indentation in INI files
        # Source: https://stackoverflow.com/questions/60874524/how-can-i-get-indentation-to-be-included-in-a-
        # multiline-value-when-read-from-con
        if parm_from_file():
            t = re.sub(r"^\|", tabul, line, flags=re.MULTILINE)
            buffer[-1] = t
            break

    return "\n".join(buffer)  # + "\n" if (len(buffer) > 0 and len(buffer[0]) > 0) else ""


def relative_mkdir(name="", as_pymodule=True):
    if name:
        Path(name).mkdir(parents=True, exist_ok=True)
        if as_pymodule:
            init_file_path = f"{os.getcwd()}/{name}/__init__.py"
            if not (os.path.isfile(init_file_path) or os.path.isdir(init_file_path)):
                open(init_file_path, 'w').close()


def append_to_file(file_path):
    return open(file_path, "a" if os.path.isfile(file_path) else "w")


def cast_variables(ref_name, var_type=''):
    return ref_name if (not var_type or var_type in ['str', 'string', 'text', 'txt']) else \
        f"{var_type}({ref_name}{ ' or 0' if var_type in ['int', 'float'] else ''})"


def cast_variables_v2(ref_name, var_type=''):
    # if var_type in ['str', 'string', 'text', 'txt']:
    #     return ref_name
    # elif var_type in ['int', 'float']:
    #     return f"{ref_name}.replace(np.nan, 0).astype({var_type}).values"

    return ref_name


def return_type_val(var_type):
    if var_type == 'int':
        return 0
    elif var_type == 'float':
        return 0.0
    else:
        return "''"


def tokenize(field):  # field looks like: "age:int" or "percent (num):float" or "account owner"
    field_name = field  # Looks like: "account owner"

    characters_to_remove = '[\\' + "\\".join(group_syms) + "]"
    tokenized_var_name = re.sub(r'\s+|_', ' ', re.sub(characters_to_remove, ' ', field_name)).strip().split(' ')
    var_name_normalized = '_'.join(get_all_shortened(tokenized_var_name))  # Looks like: account_owner

    return field_name, var_name_normalized


def clear_output(path='/generated'):
    import shutil
    file_path = os.getcwd() + path

    for filename in os.listdir(file_path):
        print(filename)
        file_path = os.path.join(file_path, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            raise e


def filename_without_ext(filename):
    import re

    return '' if filename is None else \
        (re.sub(r"\.(.*)", '', filename) if re.search(r"^Placemat", filename) else None)


def filenames_without_ext(directory):
    return list(
        filter(
            lambda file_name: file_name is not None and re.search(r'^Placemat', file_name),
            map(
                lambda filename: filename_without_ext(filename),
                os.listdir(f"{os.getcwd()}/{directory}")
            )
        )
    )


def create_variables(field_list=None, type_list=None):
    fields = map(lambda column_name: column_name.strip(), field_list.split(','))
    types = map(lambda type_name: type_name.strip(), type_list.split(','))
    declarations = []
    varnames = []
    fieldlist = []

    if field_list:
        print("List of variables:\n")

        for (field, var_type) in zip(fields, types):
            field_name, var_name_normalized = tokenize(field)
            ref = cast_variables(f"row['{field_name}']", var_type)  # Looks like: "row['name']" or "float(...)"
            declarations.append(f"self.{var_name_normalized} = {ref}")
            # declarations.append(f"except ValueError: {var_name_normalized} = {return_type_val(var_type)}\n")
            fieldlist.append(field_name)
            varnames.append(var_name_normalized)
            print(f"- {var_name_normalized}")

    print("------------------------")

    return declarations, varnames, fieldlist


def create_variables_multith_support(field_list=None, type_list=None):
    fields = map(lambda column_name: column_name.strip(), field_list.split(','))
    types = map(lambda type_name: type_name.strip(), type_list.split(','))
    castings = []
    references = []
    fieldlist = []

    if field_list:
        print("List of variables:\n")

        for (field, var_type) in zip(fields, types):
            field_name, variable_name = tokenize(field)
            ref = cast_variables_v2(f"df['{field_name}']", var_type)  # Looks like: df['col1'] = int(df['col1'])
            castings.append(f"{variable_name} = {ref}\n")
            fieldlist.append(field_name)
            references.append(variable_name)
            print(f"- {variable_name}")

    print("------------------------")

    return castings, references, fieldlist


def tab(n=1, car="    "):
    return car * n


def cmd_parm_reader():
    """Read parameters from file specified in command line.

    Source: https://stackoverflow.com/questions/28420357/python-script-command-line-arguments-from-file
    """

    if parm_from_file():
        parm_file_name = sys.argv[1]
        if path.exists(parm_file_name):
            config = configparser.ConfigParser()
            config.read(parm_file_name)
            return config
        else:
            raise FileNotFoundError(f"Parameter file '{parm_file_name}' doesn't exist.")
    return None


def input_wrapper(section, key, default_prompt_msg):
    parm_reader = cmd_parm_reader()

    return input(default_prompt_msg) if not parm_reader else parm_reader[section][key]


def key_exists(section: str = "", key: str = ""):
    parm_reader = cmd_parm_reader()

    if section not in parm_reader.keys():
        return False

    if key not in parm_reader[section].keys():
        return False

    return True


def has_section(section=""):
    parm_reader = cmd_parm_reader()

    if not (section and parm_reader):
        return False

    try:
        return parm_reader[section]
    except KeyError:
        return False


def replace_placeholders(body_lines, declarations):
    return


def parm_from_file():
    return len(sys.argv) == 2


def bulk_replace(string, patterns, repls):
    """Replace multiple substrings at once.

    Source: https://stackoverflow.com/questions/6116978/how-to-replace-multiple-substrings-of-a-string
    """

    if not patterns:
        return string

    # Two list to a dict
    # Source: https://stackoverflow.com/questions/209840/convert-two-lists-into-a-dictionary
    rep = dict(zip(patterns, list(map(lambda x: 'self.'+x, repls))))

    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))
    return pattern.sub(lambda m: rep[re.escape(m.group(0))], string)


def bulk_replace_v2(string, patterns):
    """Replace multiple substrings at once.
    FOR NOW: NOT USED.
    Source: https://stackoverflow.com/questions/6116978/how-to-replace-multiple-substrings-of-a-string
    """

    if not patterns:
        return string

    # Two list to a dict
    # Source: https://stackoverflow.com/questions/209840/convert-two-lists-into-a-dictionary
    rep = dict(zip(patterns, list(map(lambda x: f"self.row['{x}']", patterns))))

    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))
    return pattern.sub(lambda m: rep[re.escape(m.group(0))], string)


def bulk_replace_v3(string, patterns, repls):
    """Replace multiple substrings at once.

    Source: https://stackoverflow.com/questions/6116978/how-to-replace-multiple-substrings-of-a-string
    """

    if not patterns:
        return string

    # Two list to a dict
    # Source: https://stackoverflow.com/questions/209840/convert-two-lists-into-a-dictionary
    rep = dict(zip(patterns, list(map(lambda x: x, repls))))

    rep = dict((re.escape(k), v) for k, v in rep.items())
    pattern = re.compile("|".join(rep.keys()))
    return pattern.sub(lambda m: rep[re.escape(m.group(0))], string)

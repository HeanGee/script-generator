import re

import Helpers

# Regex for key string pattern
is_play = re.compile(r"^play")
is_declaration = re.compile(r"^declarations")
is_body_lines = re.compile(r"body_lines")
is_output = re.compile(r"output")
is_output_method = re.compile(r"output_method")

# Output folder name
output_folder = '../oracle-sqls'

# Symbols
math_symbols = ['+', '-', '*', '/', '%', '^']
logical_oper = ['>', '<', 'and', 'or', 'not']


class Placemat:
    """Generate a class containing all plays and logic required to create a placemat."""

    def __init__(self):
        """No declaration needed for the moment"""
        pass

    @staticmethod
    def generate(placemat_name='', new_fields=None):
        """Main function."""

        if placemat_name == '':
            placemat_name = Helpers.input_wrapper('placemat', 'name', 'Placemat Name: ')

        placemat_shortname = Helpers.input_wrapper('placemat', 'shortname', 'Placemat Short Name: ')
        placemat_name = re.sub(r'\s+', '_', placemat_name)
        template = Placemat.__construct_template(new_fields, placemat_shortname)

        # Print the template json to the console
        import json
        print(json.dumps(template, indent=2))

        # Create output folder
        Helpers.relative_mkdir(output_folder)

        # Create Placemat class file
        f = open(f"{output_folder}/Placemat{placemat_name}.py", "w+")

        # Write class definition line
        f.write('import numpy as np\n')
        f.write('import pandas as pd\n')
        f.write('from typing import Union\n')
        f.write('from utils import set_output_fields_v3, calculate_segment, limit_value\n\n\n')
        f.write(f"class {placemat_name}:\n")

        # TODO: Constructor will not be needed
        # Write constructor definition
        f.write(f"\n{Helpers.tab()}def __init__(self, df: pd.DataFrame):")
        f.write(f"\n{Helpers.tab(n=2)}self.df = df")

        # Initial tasks before call plays
        Placemat.__define_initial_tasks(file=f)

        # TODO: Only receives df and casts their values
        # Define method to update the row, headers and variables
        f.write(f"\n\n{Helpers.tab()}def generate_plays(self):")
        for play in template:
            play_field = template[play]['name_of_play']
            acv_field = template[play]['name_of_acv']
            f.write(f"\n{Helpers.tab(n=2)}self.df = set_output_fields_v3(self.df, '{play_field}')")
            f.write(f"\n{Helpers.tab(n=2)}self.df = set_output_fields_v3(self.df, '{acv_field}')")
        f.write('\n')

        # Call to plays and return updated dataframe
        f.write(f"\n{Helpers.tab(n=2)}self.updates_play_fields()")
        f.write(f"\n\n{Helpers.tab(n=2)}return self.df")

        # Write generic automator method
        f.write(f"\n\n{Helpers.tab()}def updates_play_fields(self):")
        for key, _ in template.items():
            if is_play.match(key):
                name_of_play = template[key]['name_of_play']
                name_of_acv = template[key]['name_of_acv']
                f.write(f"\n{Helpers.tab(n=2)}self.df['{name_of_play}'] = self.df.apply({key}, axis=1, which='play')")
                f.write(f"\n{Helpers.tab(n=2)}self.df['{name_of_acv}'] = self.df.apply({key}, axis=1, which='acv')")

        # Write methods for plays
        Placemat.__write(template, f)

        # Write custom method
        Placemat.__write_methods(file=f, template=template)
        f.close()

    @staticmethod
    def __cast_dataframe_values(template=None, f=None):
        casting_list = []  # control duplicates
        for play in template:
            if re.match('^play', play):
                for casting in template[play]['castings']:
                    if casting not in casting_list:
                        f.write(f"\n{Helpers.tab(n=2)}{casting}")
                        casting_list.append(casting)

    @staticmethod
    def __write_methods(file=None, template=None):
        if not file:
            return

        if Helpers.has_section('methods'):
            method = Helpers.paragraph_input(section='methods', key='code', tabul=Helpers.tab(1))
            method = Helpers.bulk_replace(method, template['fieldlist'], template['varnames'])
            file.write(f"\n\n{method}")

    @staticmethod
    def __define_initial_tasks(file=None):
        if not file:
            return

        if Helpers.has_section('initial-tasks'):
            tasks = Helpers.paragraph_input(section='initial-tasks', key='code', prompt_header="Define initial tasks",
                                            tabul=Helpers.tab(2))
            file.write("\n\n" + re.sub('$\n', '', tasks)) if len(tasks) > 4 else None

    @staticmethod
    def __construct_template(variable_list=None, placemat_shortname=None):
        """Creates a JSON such that facilitates the generation of Placemat class file."""

        play_quantity = int(Helpers.input_wrapper('placemat', 'plays', 'Amount of Plays: '))
        template = {}
        fieldlist = []
        references = []
        for playOrder in range(play_quantity):
            print()
            playOrder += 1
            play_name = Helpers.input_wrapper(f"play{playOrder}", 'name',
                                              "Play Name: ").strip() or f"Play{playOrder + 1}"
            print("\n========================")
            print(f"Set up Play #{playOrder}: {play_name}")
            print("========================")

            # Crear variables desde parametro. Usar variable creator con el trycatcher
            field_list = Helpers.input_wrapper(f"play{playOrder}", 'fields',
                                               "Specify new field names separated by comma: ")
            type_list = Helpers.input_wrapper(f"play{playOrder}", 'types',
                                              "Specify the types separated by comma  (int, char, float): ")
            castings, references, field_list = Helpers.create_variables_multith_support(field_list=field_list,
                                                                                        type_list=type_list)
            declarations = ['play = "No"\n', 'play_acv = 0\n']
            declarations += castings

            # Permitir introducir parrafos
            _prompt_header = "Introduce python code (use 'play_acv' as variable name for the acv calculation)"
            body_lines = Helpers.paragraph_input(section=f"play{playOrder}",
                                                 prompt_header=_prompt_header,
                                                 tabul=Helpers.tab(1))

            # Reemplazar los placeholders de la formula por sus respectivas variables
            body_lines = Helpers.bulk_replace_v3(body_lines, field_list, references).replace('self.', '')

            # Stack field list and variable names for later use.
            if field_list:
                fieldlist.append(field_list)
            if references:
                references.append(references)

            # Set output
            output = "return play if which == 'play' else play_acv"

            # Armar la play
            play = {
                'name': play_name, 'declarations': declarations, 'fieldlist': field_list,
                'castings': castings, 'body_lines': body_lines, 'output': output,
                'name_of_play': f"{placemat_shortname} {play_name}",
                'name_of_acv': f"{placemat_shortname} {play_name} ACV"
            }

            template[f'play{playOrder}'] = play

        # template['fieldlist'] = list(itertools.chain.from_iterable(fieldlist))
        # template['references'] = list(itertools.chain.from_iterable(references))

        return template

    @staticmethod
    def __write(template, f):
        """Write to disk in python script format he template received."""

        for key, value in template.items():
            if is_play.match(key):
                f.write(f'\n\n\n# {value["name"]}\n')
                f.write(f'def {key}(df, which: str = None) -> Union[str, int, float]:\n')

                Placemat.__write(value, f)
            elif is_declaration.match(key):
                for declaration in value:
                    f.write(Helpers.tab() + declaration)
                f.write('\n')
            elif is_body_lines.match(key):
                f.write(value)
                f.write('\n')
            elif is_output.match(key):
                f.write(Helpers.tab() + value)
            elif is_output_method.match(key):
                f.write(value)


if __name__ == "__main__":
    try:
        Placemat.generate()
    except Exception as e:
        print('==============================')
        print("Exception raised.\nReason:\n")
        raise e

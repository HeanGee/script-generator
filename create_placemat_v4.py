import re

import Helpers

# Regex for key string pattern
is_play = re.compile(r"^play")
is_declaration = re.compile(r"^declarations")
is_body_lines = re.compile(r"body_lines")
is_output_method = re.compile(r"output_method")

# Output folder name
output_folder = '../oracle-sqls'

# Symbols
math_symbols = ['+', '-', '*', '/', '%', '^']
logical_oper = ['>', '<', 'and', 'or', 'not']


class Placemat:
    """Generate a class containing all plays and logic required to create a placemat."""

    def __init__(self):
        """No declaration needed for the moment"""
        pass

    @staticmethod
    def generate(placemat_name='', new_fields=None):
        """Main function."""

        if placemat_name == '':
            placemat_name = Helpers.input_wrapper('placemat', 'name', 'Placemat Name: ')

        placemat_shortname = Helpers.input_wrapper('placemat', 'shortname', 'Placemat Short Name: ')
        placemat_name = re.sub(r'\s+', '_', placemat_name)
        template = Placemat.__construct_template(new_fields, placemat_shortname)
        play_amount = Helpers.input_wrapper('placemat', 'plays', "Active Plays (separated by commas): ").strip()
        active_plays = Helpers.input_wrapper('placemat', 'active-plays', "Active Plays (separated by commas): ").strip()
        if active_plays == "all":
            active_plays = list(map(lambda x: f'{x}', list(range(1, int(play_amount) + 1))))
        else:
            active_plays = list(map(lambda ap: ap.strip(), active_plays.split(sep=',')))

        # Print the template json to the console
        import json
        print(json.dumps(template, indent=2))

        # Create output folder
        Helpers.relative_mkdir(output_folder)

        # Create Placemat class file
        f = open(f"{output_folder}/Placemat{placemat_name}.py", "w+")

        # Write class definition line
        f.write('import numpy as np\n')
        f.write('import pandas as pd\n')
        f.write('import random\n')
        f.write('import threading\n')
        f.write('import traceback\n')
        f.write('from queue import Empty, Queue\n')
        f.write('from typing import Union, List\n')
        f.write('from utils import calculate_segment, limit_value, progress_bar_with_time\n')
        f.write('random.seed()\n\n\n')
        f.write(f"class {placemat_name}(threading.Thread):")

        f.write(f"\n{Helpers.tab()}thid = -1\n")
        f.write(f"\n{Helpers.tab()}AMOUNT_OF_PLAYS = {play_amount}\n")
        fields = []
        play_fields = []
        acv_fields = []
        placemat_fields = []
        operation_fields = []
        map_play_name_method = {}
        extra_fields = {}
        for play, _ in template.items():
            if is_play.match(play):
                if not play.replace("play", "") in active_plays:  # Only active plays
                    continue

                name_of_play = template[play]['name_of_play']
                name_of_acv = template[play]['name_of_acv']

                play_fields += [name_of_play]
                acv_fields += [name_of_acv]
                placemat_fields += play_fields + acv_fields
                operation_fields += template[play]['fieldlist']

                fields += operation_fields + play_fields + acv_fields

                map_play_name_method[name_of_play] = play

                if template[play]['extra-fields']:
                    play_name = template[play]['name_of_play']
                    extra_fields[play_name] = template[play]['extra-fields']

        fields = list(set(fields))
        fields.sort()
        acv_fields = list(set(acv_fields))
        acv_fields.sort()
        play_fields = list(set(play_fields))
        play_fields.sort()
        placemat_fields = list(set(placemat_fields))
        placemat_fields.sort()
        operation_fields = list(set(operation_fields))
        operation_fields.sort()

        fields = ', '.join(map(lambda field: f"'{field}'", fields))
        acv_fields = ', '.join(map(lambda field: f"'{field}'", acv_fields))
        play_fields = ', '.join(map(lambda field: f"'{field}'", play_fields))
        placemat_fields = ', '.join(map(lambda field: f"'{field}'", placemat_fields))
        operation_fields = ', '.join(map(lambda field: f"'{field}'", operation_fields))

        # Class variables
        f.write(f"\n{Helpers.tab()}FIELDNAMES = [{fields}]\n")
        f.write(f"\n{Helpers.tab()}PLAY_FIELDS = [{play_fields}]\n")
        f.write(f"\n{Helpers.tab()}ACVS_FIELDS = [{acv_fields}]\n")
        f.write(f"\n{Helpers.tab()}PLACEMAT_FIELDS = [{placemat_fields}]\n")
        f.write(f"\n{Helpers.tab()}OPERATION_FIELDS = [{operation_fields}]\n")
        f.write(f"\n{Helpers.tab()}MAP_PLAY_MTH = {map_play_name_method}\n")
        f.write(f"\n{Helpers.tab()}EXTRA_FIELDS = {extra_fields}\n")

        # Constructor
        f.write(
            f"\n{Helpers.tab()}def __init__(self, df: pd.DataFrame, reset_th_id=False, line: int = 0, "
            f"event: threading.Event = None):")
        f.write(f"\n{Helpers.tab(n=2)}super().__init__()\n")
        f.write(f"\n{Helpers.tab(n=2)}self.id = self.__generate_thread_id(reset_th_id=reset_th_id)\n")
        f.write(f"\n{Helpers.tab(n=2)}self.df = df")
        f.write(f"\n{Helpers.tab(n=2)}self.line = line")
        f.write(f"\n{Helpers.tab(n=2)}self.event = event")
        f.write(f"\n{Helpers.tab(n=2)}self.name = '{placemat_name}'")
        f.write(f"\n{Helpers.tab(n=2)}self.queue = Queue()")
        f.write(f"\n{Helpers.tab(n=2)}self.workers: List[PlayCrawler] = []\n")

        # Instance methods
        f.write(f"\n{Helpers.tab()}def __reset_th_id(self):")
        f.write(f"\n{Helpers.tab(n=2)}self.__class__.thid = -1\n")
        f.write(f"\n{Helpers.tab()}def __generate_thread_id(self, reset_th_id=False):")
        f.write(f"\n{Helpers.tab(n=2)}self.__reset_th_id() if reset_th_id else None")
        f.write(f"\n{Helpers.tab(n=2)}self.__increment_thid()")
        f.write(f"\n{Helpers.tab(n=2)}return self.__class__.thid\n")
        f.write(f"\n{Helpers.tab()}def __increment_thid(self):")
        f.write(f"\n{Helpers.tab(n=2)}self.thid += 1\n")
        f.write(f"\n{Helpers.tab()}def get_th_id(self):")
        f.write(f"\n{Helpers.tab(n=2)}return self.id\n")

        # Thread RUN method
        f.write(f"\n{Helpers.tab()}def run(self):")
        f.write(
            f"\n{Helpers.tab(n=2)}# self.df = extender(df=self.df, line=self.line, event=self.event, "
            f"queue=self.queue)\n")
        f.write(
            f"\n{Helpers.tab(n=2)}for playname in {placemat_name}.PLAY_FIELDS:  # One thread for each play")
        f.write(f"\n{Helpers.tab(n=3)}worker = PlayCrawler(queue=self.queue, event=self.event)")
        f.write(f"\n{Helpers.tab(n=3)}worker.daemon = True")
        f.write(f"\n{Helpers.tab(n=3)}worker.start()\n")
        f.write(f"\n{Helpers.tab(n=3)}self.workers.append(worker)\n")
        f.write(f"\n{Helpers.tab(n=3)}funct_nm = {placemat_name}.MAP_PLAY_MTH[playname]")
        f.write(f"\n{Helpers.tab(n=3)}self.queue.put((self.df, globals()[funct_nm], playname))\n")
        # f.write(f"\n{Helpers.tab(n=2)}# Spawn Thread as many as play they are")
        # f.write(f"\n{Helpers.tab(n=2)}for i in range(len({placemat_name}.PLAY_FIELDS)):")
        # f.write(f"\n{Helpers.tab(n=3)}playname = {placemat_name}.PLAY_FIELDS[i]")
        # f.write(f"\n{Helpers.tab(n=3)}funct_nm = {placemat_name}.MAP_PLAY_MTH[playname]\n")
        # f.write(f"\n{Helpers.tab(n=3)}self.queue.put((self.df, globals()[funct_nm], playname))\n")
        f.write(f"\n{Helpers.tab(n=2)}# Wait the thread until all items is consumed.")
        f.write(f"\n{Helpers.tab(n=2)}self.queue.join()\n")
        f.write(f"\n{Helpers.tab(n=2)}return self.df\n\n")

        # Initial tasks
        Placemat.__define_initial_tasks(f)

        # Thread Class spawned by placemat Thread.
        f.write(f"\n{Helpers.tab(n=0)}class PlayCrawler(threading.Thread):")

        # Constructor
        f.write(f"\n{Helpers.tab(n=1)}def __init__(self, queue: Queue, event: threading.Event, name: str = ''):")
        f.write(f"\n{Helpers.tab(n=2)}threading.Thread.__init__(self)")
        f.write(f"\n{Helpers.tab(n=2)}self.event = event")
        f.write(f"\n{Helpers.tab(n=2)}self.queue = queue")
        f.write(f"\n{Helpers.tab(n=2)}self.name = f'CRAWLER-{{name or random.randrange(1, 11, 1)}}'")
        f.write(f"\n{Helpers.tab(n=2)}self.exception = None")
        f.write(f"\n{Helpers.tab(n=2)}self.id = self.name\n")

        # Thread RUN method
        f.write(f"\n{Helpers.tab(n=1)}def run(self):")
        f.write(f"\n{Helpers.tab(n=2)}while True:")
        f.write(f"\n{Helpers.tab(n=3)}# Get the work from the queue and expand the n-ple")
        f.write(f"\n{Helpers.tab(n=3)}df, function, play = self.queue.get()")
        f.write(f"\n{Helpers.tab(n=3)}self.name = play\n")
        f.write(f"\n{Helpers.tab(n=3)}try:")
        f.write(f"\n{Helpers.tab(n=4)}if self.event.is_set():")
        f.write(f"\n{Helpers.tab(n=5)}function(df, event=self.event)")
        f.write(f"\n{Helpers.tab(n=3)}except Exception as e:")
        f.write(f"\n{Helpers.tab(n=4)}self.exception = e")
        f.write(f"\n{Helpers.tab(n=4)}with open(f'generated/[log] {{self.name}}.txt', 'a') as f:")
        f.write(f"\n{Helpers.tab(n=5)}f.write(str(e))")
        f.write(f"\n{Helpers.tab(n=5)}f.write(traceback.format_exc())")
        f.write(f"\n{Helpers.tab(n=3)}finally:")
        f.write(f"\n{Helpers.tab(n=4)}self.queue.task_done()")

        # Write methods for plays
        Placemat.__write(template, f)

        # Write custom method
        Placemat.__write_methods(file=f, template=template)
        f.close()

    @staticmethod
    def __write_methods(file=None, template=None):
        if not file:
            return

        if Helpers.has_section('methods'):
            method = Helpers.paragraph_input(section='methods', key='code', tabul=Helpers.tab(1))
            method = Helpers.bulk_replace(method, template['fieldlist'], template['varnames'])
            file.write(f"\n\n{method}")

    @staticmethod
    def __define_initial_tasks(file=None):
        if not file:
            return

        if Helpers.has_section('initial-tasks'):
            tasks = Helpers.paragraph_input(section='initial-tasks', key='code', prompt_header="Define initial tasks",
                                            tabul=Helpers.tab(n=0))
            file.write(f"\n{tasks}\n\n") if len(tasks) > 4 else None

    @staticmethod
    def __construct_template(variable_list=None, placemat_shortname=None):
        """Creates a JSON such that facilitates the generation of Placemat class file."""

        play_quantity = int(Helpers.input_wrapper('placemat', 'plays', 'Amount of Plays: '))
        template = {}
        fieldlist = []
        for playOrder in range(play_quantity):
            print()
            playOrder += 1
            play_name = Helpers.input_wrapper(f"play{playOrder}", 'name',
                                              "Play Name: ").strip() or f"Play{playOrder + 1}"
            print("\n========================")
            print(f"Set up Play #{playOrder}: {play_name}")
            print("========================")

            # Crear variables desde parametro. Usar variable creator con el trycatcher
            field_list = Helpers.input_wrapper(f"play{playOrder}", 'fields',
                                               "Specify new field names separated by comma: ")
            type_list = Helpers.input_wrapper(f"play{playOrder}", 'types',
                                              "Specify the types separated by comma  (int, char, float): ")
            castings, references, field_list = Helpers.create_variables_multith_support(field_list=field_list,
                                                                                        type_list=type_list)
            declarations = []  # ['play = "No"\n', 'play_acv = 0\n']
            declarations += castings

            play_cond = Helpers.paragraph_input(section=f"play{playOrder}",
                                                prompt_header="Introduce python code",
                                                tabul=Helpers.tab(0))

            acv = Helpers.paragraph_input(section=f"play{playOrder}", key='acv',
                                          prompt_header="Introduce python code",
                                          tabul=Helpers.tab(0))
            acv = f'\n{Helpers.tab(1)}'.join(acv.split('\n'))

            variables = Helpers.paragraph_input(section=f"play{playOrder}", key='vars',
                                                prompt_header="Introduce python code",
                                                tabul=Helpers.tab(0))

            body_lines = ''

            # Vars
            if variables:
                body_lines = Helpers.tab(1) + f'\n{Helpers.tab(1)}'.join(
                    list(map(lambda x: x.strip(), variables.split('\n')))) + '\n\n'

            # Play condition
            body_lines += f'{Helpers.tab(1)}play_cond = {play_cond}\n\n'

            # Flag column
            body_lines += f'{Helpers.tab(1)}df["{placemat_shortname} {play_name}"] = "No"\n'
            body_lines += f"{Helpers.tab(1)}df.loc[play_cond, '{placemat_shortname} {play_name}'] = 'Yes'\n\n"

            # ACV column
            body_lines += f"{Helpers.tab(1)}df['{placemat_shortname} {play_name} ACV'] = 0\n"
            body_lines += Helpers.tab(1) + Helpers.bulk_replace(
                acv, ['play-cond', 'acv-field'], ['play_cond', f'"{placemat_shortname} {play_name} ACV"']
            )

            # Replace column names by variable declarations
            body_lines = Helpers.bulk_replace_v3(body_lines, field_list, references)

            # Find and replace all holders with un-replaced field references.
            if Helpers.key_exists(section=f"play{playOrder}", key='holders'):
                holders = Helpers.paragraph_input(section=f"play{playOrder}", key='holders', prompt_header="")
                for i in range(int(holders)):
                    i += 1
                    holder = Helpers.paragraph_input(section=f"play{playOrder}", key=f'holder{i}')
                    body_lines = Helpers.bulk_replace_v3(body_lines, [f'holder{i}'], [holder])

            # Reemplazar las llaves por parentesis
            body_lines = body_lines \
                .replace('self.', '') \
                .replace('} and {', '} & {') \
                .replace('} or {', '} | {') \
                .replace('} and', '} &') \
                .replace('} or', '} |') \
                .replace('and {', '& {') \
                .replace('or {', '| {') \
                .replace('{', '(') \
                .replace('}', ')')

            # Stack field list and variable names for later use.
            if field_list:
                fieldlist.append(field_list)
            if references:
                references.append(references)

            # Adds extra fields
            try:
                extra_fields = Helpers.input_wrapper(f'play{playOrder}', 'extra-fields', '').split(',')
                extra_fields = [ef.strip() for ef in extra_fields]

            except KeyError:
                extra_fields = None

            # Armar la play
            play = {
                'name': play_name, 'declarations': declarations, 'fieldlist': field_list,
                'castings': castings, 'body_lines': body_lines,
                'name_of_play': f"{placemat_shortname} {play_name}",
                'name_of_acv': f"{placemat_shortname} {play_name} ACV",
                'extra-fields': extra_fields
            }

            template[f'play{playOrder}'] = play

        return template

    @staticmethod
    def __write(template, f):
        """Write to disk in python script format he template received."""

        for key, value in template.items():
            if is_play.match(key):
                f.write(f'\n\n\n# {value["name"]}\n')
                f.write(f'def {key}(df, event: threading.Event = None):')
                f.write(f'\n{Helpers.tab(n=1)}if not event.is_set():')
                f.write(f'\n{Helpers.tab(n=2)}return\n\n')

                Placemat.__write(value, f)
            elif is_declaration.match(key):
                for declaration in value:
                    f.write(Helpers.tab() + declaration)
                f.write('\n')
            elif is_body_lines.match(key):
                f.write(value)
            elif is_output_method.match(key):
                f.write(value)


if __name__ == "__main__":
    try:
        Placemat.generate()
    except Exception as e:
        print('==============================')
        print("Exception raised.\nReason:\n")
        raise e

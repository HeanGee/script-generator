import unittest

import Helpers


class TestHelpers(unittest.TestCase):
    def setUp(self):
        self.fake_token_list = ['ORDER', 'Status', 'lightning', 'qUantiTY', 'pEdRO']
        self.expected_token_list = ['ord', 'status', 'light', 'qty', 'pedr']
        self.non_expected_token_list = ['Ord', 'Status', 'Light', 'Qty', 'pEdR']

    def test_get_short(self):
        #  Good scenario
        [self.assertEqual(expected, returned, f"expected '{expected}' but returned '{returned}'")
         for (expected, returned) in
         zip(self.expected_token_list, map(lambda x: Helpers.get_short(x), self.fake_token_list))]

        #  Bad Scenario
        [self.assertNotEqual(unexpected, returned, f"") for (unexpected, returned) in
         zip(self.non_expected_token_list, map(lambda x: Helpers.get_short(x), self.fake_token_list))]

    def test_get_all_shortened(self):
        result = Helpers.get_all_shortened(self.fake_token_list)
        [self.assertEqual(expected_token, returned_token, f"'{fake_token}' haven't parsed correctly")
         for (expected_token, returned_token, fake_token) in
         zip(self.expected_token_list, result, self.fake_token_list)]

    def test_create_variables(self):
        field_list = "Core Org Status,Marketing AOV Segment (Numeric)"
        type_list = ",int"
        expected_output = {
            "declaration": ["self.core_org_status = row['Core Org Status']"],
            "var_names": ["core_org_status", "mrktng_aov_segm_num"],
            "field_list": ["Core Org Status", "Marketing AOV Segment (Numeric)"]
        }
        returned_decl, returned_var_names, returned_fieldlist = Helpers.create_variables(field_list=field_list, type_list=type_list)
        returned_output = {
            "declaration": list(map(lambda declaration: declaration.strip(), returned_decl)),
            "var_names": returned_var_names,
            "field_list": returned_fieldlist
        }

        [
            [
                self.assertEqual(expected, returned)
                for (expected, returned)
                in zip(expected_output[key1], returned_output[key2])
            ]
            for (key1, key2)
            in zip(['declaration', 'var_names', 'field_list'], ['declaration', 'var_names', 'field_list'])
        ]

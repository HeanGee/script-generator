# Welcome to Placemat Builder Code Generator 👋
A simple code generator to automate the placemats generation.


## Requirements
```
pip3 install requirements.txt
```

## Usage
****
```sh
python3 create_placemat_v<version>.py inputs/<.ini file>
```

**where the file `<.ini file>` contains:**
```shell script
[placemat]
name = PLACEMAT_NAME
shortname = SHORTENED_PLACEMAT_NAME
plays = 3

[play1]
name = PLAY 1 NAME
fields = Core Org Status, Order Quantity, PTB Segment Numeric
types = , int, float
play = |print("TBD")

[play2]
name = PLAY 2 NAME
fields = Core Org Status, Order Quantity, PTB Segment Numeric
types = , int, float
play = |if Core Org Status == 0 and Order Quantity == 0:
       |    play = 'Yes'
       |    play_acv = PTB Segment Numeric

[play3]
name = PLAY 3 NAME
fields = Core Org Status, Order Quantity, PTB Segment Numeric
types = , int, float
play = |if Core Org Status == 0 and Order Quantity == 0:
       |    play = 'Yes'
       |    play_acv = PTB Segment Numeric
```

## Utilities
#### Unittest
```
pytest test/test-file-name
```
#### Coverage and Uniitest
```
coverage run -m pytest && coverage report
```
#### Result into a file
```
coverage run -m pytest && coverage report > coverage.txt
```

## Author

👤 **민 현 기**

* Gitlab: [@HeanGee](https://gitlab.com/HeanGee)
* LinkedIn: [@daniel-min-590708112](https://linkedin.com/in/daniel-min-590708112)

## Show your support

Give a ⭐️ if this project helped you!


***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
import itertools
import re

import Helpers

# Regex for key string pattern
is_play = re.compile(r"^play")
is_declaration = re.compile(r"^declarations")
is_body_lines = re.compile(r"body_lines")
is_output = re.compile(r"output")
is_output_method = re.compile(r"output_method")

# Output folder name
output_folder = '../prospecting-placemats'

# Symbols
math_symbols = ['+', '-', '*', '/', '%', '^']
logical_oper = ['>', '<', 'and', 'or', 'not']


class SFReportFieldExtractor:
    """Generates a file containing a list of variables/fields to be used for a specific Placemat."""

    def __init__(self):
        """No needed for any declaration for the moment"""
        pass

    @staticmethod
    def load_fields():
        placemat_name = input("Placemat Name: ").strip()
        field_list = input("Specify new field names separated by comma: ")
        type_list = input("Specify the types separated by comma  (int, char, float): ")

        fields = map(lambda column_name: column_name.strip(), field_list.split(','))

        # Open/Create file to append them
        Helpers.relative_mkdir(output_folder)
        file_path = f"{output_folder}/extracted_columns.py"
        file = Helpers.append_to_file(file_path)

        # Add through comment which placemat this fields are for
        file.write(f"{Helpers.tab(n=3)}# For {placemat_name} placemat.\n")

        new_fields = []

        # Write main code
        for (field, var_type) in zip(fields, type_list):
            # field looks like: "age:int" or "percent:float" or "account owner"
            field_name, var_name_normalized = Helpers.tokenize(field)
            ref = Helpers.cast_variables(f"row['{field_name}']", var_type)  # Looks like: "row['name']" or "float(...)"
            file.write(f"{Helpers.tab(n=3)}try: {var_name_normalized} = {ref}\n")
            file.write(f"{Helpers.tab(n=3)}except ValueError: {var_name_normalized} = "
                       f"{Helpers.return_type_val(var_type)}\n")
            new_fields.append(var_name_normalized)

        # Add empty line
        file.write(f"{Helpers.tab(n=3)}\n")
        file.close()

        print("==========================")
        print("New variables added:")
        print("--------------------")
        print()
        print("- " + '\n- '.join(new_fields))
        print()
        print("==========================")

        return placemat_name, new_fields


class Placemat:
    """Generate a class containing all plays and logic required to create a placemat."""

    def __init__(self):
        """No declaration needed for the moment"""
        pass

    @staticmethod
    def generate(placemat_name='', new_fields=None):
        """Main function."""

        if placemat_name == '':
            placemat_name = Helpers.input_wrapper('placemat', 'name', 'Placemat Name: ')

        placemat_shortname = Helpers.input_wrapper('placemat', 'shortname', 'Placemat Short Name: ')
        placemat_name = re.sub(r'\s+', '_', placemat_name)
        template = Placemat.__construct_template(new_fields, placemat_shortname)

        # Print the template json to the console
        import json
        print(json.dumps(template, indent=2))

        # Create output folder
        Helpers.relative_mkdir(output_folder)

        # Create Placemat class file
        f = open(f"{output_folder}/Placemat{placemat_name}.py", "w+")

        # Write class definition line
        f.write('from utils import calculate_segment, limit_value, set_if_zero_or_none, set_output_fields, set_target'
                '\n\n\n')
        f.write(f"class {placemat_name}:\n")

        # Write constructor definition
        f.write(f"\n{Helpers.tab()}def __init__(self):")
        f.write(f"\n{Helpers.tab(n=2)}self.row = None")
        f.write(f"\n{Helpers.tab(n=2)}self.headers = None\n")
        Placemat.__declare_properties(template=template, f=f)

        # Initial tasks before call plays
        Placemat.__define_initial_tasks(file=f)

        # Define method to update the row, headers and variables
        f.write(f"\n\n{Helpers.tab()}def update_properties(self, row=None, headers=None):")
        f.write(f"\n{Helpers.tab(n=2)}self.row = row")
        f.write(f"\n{Helpers.tab(n=2)}self.headers = headers\n")
        Placemat.__update_properties(template=template, f=f)

        # Call to plays
        f.write(f"\n\n{Helpers.tab(n=2)}self.generate_plays()")

        # Write generic automator method
        f.write(f"\n\n{Helpers.tab()}def generate_plays(self):")
        for key, _ in template.items():
            if is_play.match(key):
                f.write(f"\n{Helpers.tab(n=2)}self.calc_{key}()")

        # Write methods for plays
        Placemat.__write(template, f)

        # Write custom method
        Placemat.__write_methods(file=f, template=template)
        f.write("\n")
        f.close()

    @staticmethod
    def __declare_properties(template=None, f=None):
        varnames = []
        for play in template:
            if re.match('^play', play):
                for variable in template[play]['varnames']:
                    if variable not in varnames:
                        f.write(f"\n{Helpers.tab(n=2)}self.{variable} = None")
                        varnames.append(variable)

    @staticmethod
    def __update_properties(template=None, f=None):
        declarations = []  # control duplicates
        for play in template:
            if re.match('^play', play):
                for declaration in template[play]['field_declarations']:
                    if declaration not in declarations:
                        f.write(f"\n{Helpers.tab(n=2)}{declaration}")
                        declarations.append(declaration)

    @staticmethod
    def __write_methods(file=None, template=None):
        if not file:
            return

        if Helpers.has_section('methods'):
            method = Helpers.paragraph_input(section='methods', key='code', tabul=Helpers.tab(1))
            method = Helpers.bulk_replace(method, template['fieldlist'], template['varnames'])
            file.write(f"\n\n{method}")

    @staticmethod
    def __define_initial_tasks(file=None):
        if not file:
            return

        if Helpers.has_section('initial-tasks'):
            tasks = Helpers.paragraph_input(section='initial-tasks', key='code', prompt_header="Define initial tasks",
                                            tabul=Helpers.tab(2))
            file.write("\n\n" + re.sub('$\n', '', tasks)) if len(tasks) > 4 else None

    @staticmethod
    def __construct_template(variable_list=None, placemat_shortname=None):
        """Creates a JSON such that facilitates the generation of Placemat class file."""

        play_quantity = int(Helpers.input_wrapper('placemat', 'plays', 'Amount of Plays: '))
        template = {}
        fieldlist = []
        varnames = []
        for playOrder in range(play_quantity):
            print()
            playOrder += 1
            play_name = Helpers.input_wrapper(f"play{playOrder}", 'name',
                                              "Play Name: ").strip() or f"Play{playOrder + 1}"
            print("\n========================")
            print(f"Set up Play #{playOrder}: {play_name}")
            print("========================")

            # Crear variables desde parametro. Usar variable creator con el trycatcher
            field_list = Helpers.input_wrapper(f"play{playOrder}", 'fields',
                                               "Specify new field names separated by comma: ")
            type_list = Helpers.input_wrapper(f"play{playOrder}", 'types',
                                              "Specify the types separated by comma  (int, char, float): ")
            field_declarations, var_names, field_list = Helpers.create_variables(field_list=field_list,
                                                                                 type_list=type_list)
            declarations = ['play = "No"\n', 'play_acv = 0\n', f'play_field = "{placemat_shortname} {play_name}"\n',
                            f'play_acv_field = "{placemat_shortname} {play_name} ACV"\n']

            # Permitir introducir parrafos
            body_lines = Helpers.paragraph_input(section=f"play{playOrder}", prompt_header="Introduce python code",
                                                 tabul=Helpers.tab(2))

            # Reemplazar los placeholders de la formula por sus respectivas variables
            body_lines = Helpers.bulk_replace(body_lines, field_list, var_names)

            # Stack field list and variable names for later use.
            if field_list:
                fieldlist.append(field_list)
            if var_names:
                varnames.append(var_names)

            # Set output
            output = "set_output_fields(play_field, play, play_acv_field, play_acv, self.row, self.headers)"

            # Armar la play
            play = {
                'name': play_name,
                'declarations': declarations,
                'fieldlist': field_list,
                'varnames': var_names,
                'field_declarations': field_declarations,
                'body_lines': body_lines,
                'output': output,
            }

            template[f'play{playOrder}'] = play

        template['fieldlist'] = list(itertools.chain.from_iterable(fieldlist))
        template['varnames'] = list(itertools.chain.from_iterable(varnames))

        return template

    @staticmethod
    def __write(template, f):
        """Write to disk in python script format he template received."""

        for key, value in template.items():
            if is_play.match(key):
                # f.write(f'\n\n{Helpers.tab()}@classmethod')
                f.write(f'\n\n{Helpers.tab()}# {value["name"]}\n')
                f.write(f'{Helpers.tab()}def calc_{key}(self):\n')

                Placemat.__write(value, f)
            elif is_declaration.match(key):
                for declaration in value:
                    f.write(Helpers.tab(2) + declaration)
                f.write('\n')
            elif is_body_lines.match(key):
                f.write(value)
                f.write('\n')
            elif is_output.match(key):
                f.write(Helpers.tab(2) + value)
            elif is_output_method.match(key):
                f.write(value)


if __name__ == "__main__":
    try:
        Placemat.generate()
    except Exception as e:
        print('==============================')
        print("Exception raised.\nReason:\n")
        raise e

import csv
import sys

with open(sys.argv[1], newline='') as f:
    reader = csv.reader(f)
    for row in reader:
        row.sort()
        with open('../CSV_In/Headers.csv', 'w') as file:
            file.write('\n'.join(row))
            file.close()
        break
